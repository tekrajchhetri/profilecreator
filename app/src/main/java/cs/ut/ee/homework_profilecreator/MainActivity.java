package cs.ut.ee.homework_profilecreator;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_USER_DETAIL = 2;
    ImageView imageView;
    Context context;
    TextView name;
    TextView job;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
        name = findViewById(R.id.name_user);
        job = findViewById(R.id.job_user);
        imageView = findViewById(R.id.imageView);
        context = this;
    }

    public void takePicture(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imageBitmap);
        }

        if(requestCode == REQUEST_USER_DETAIL && resultCode == RESULT_OK){
            name.setText(data.getStringExtra("name"));
            job.setText(data.getStringExtra("job"));
        }
    }



    public void details(View view) {
        Intent detailsIntent = new Intent(this, Details.class);
        startActivityForResult(detailsIntent,REQUEST_USER_DETAIL);
    }
}
