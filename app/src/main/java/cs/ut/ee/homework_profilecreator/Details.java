package cs.ut.ee.homework_profilecreator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class Details extends AppCompatActivity {

    EditText editText;
    static final int SEND_USER_DATA = 3;
    Spinner spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        spinner = findViewById(R.id.job);
        editText = findViewById(R.id.name);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.job_position,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    public void returnBack(View view) {
        String position = spinner.getSelectedItem().toString();
        String name = editText.getText().toString();
        if(name.length() == 0){
            Toast toast = Toast.makeText(this,"Enter your name",Toast.LENGTH_SHORT);
            toast.show();
        }else{
            Intent detailsIntent = new Intent(this,MainActivity.class);
            detailsIntent.putExtra("name",name);
            detailsIntent.putExtra("job",position);
            setResult(RESULT_OK, detailsIntent); //for sending back
            finish();
        }

    }

}
